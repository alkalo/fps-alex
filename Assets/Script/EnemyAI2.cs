﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class EnemyAI2 : MonoBehaviour
{
    private NavMeshAgent agent;

	void Start(){
		agent = GetComponent<NavMeshAgent> ();
	}

    public void SetTarget(Vector3 point)
    {
        agent.SetDestination(point);	
    }
}
