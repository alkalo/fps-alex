﻿using UnityEngine;
using System.Collections;

public class MisilShoot : MonoBehaviour {

	public GameObject misil_prefab;
	float misilImpulse = 20f;


	// Update is called once per frame
	public void ShotMisil () {
		GameObject themisil = (GameObject)Instantiate(misil_prefab, Camera.main.transform.position + Camera.main.transform.forward, Camera.main.transform.rotation);
		themisil.GetComponent<Rigidbody>().AddForce( Camera.main.transform.forward * misilImpulse, ForceMode.Impulse);
	}
}